# Teste para a Agencia MestreSEO

O teste consistia em fazer uma aplicação que consultasse uma API de jogos da LotoMania.

## Setup

Para executar o projeto você precisar ter o [GruntJS](http://gruntjs.com/) e o [Bower](http://bower.io/) instalados.

1. Instale os components do bower e do grunt pela linha de comando:

```sh
$ [sudo] npm install && bower install
```

2. Também precisamos rodar um comando para poder concatenar e minificar os arquivos de dependências.

```sh
$ grunt
```

Com isso ele criará uma pasta chamada `dist` em `app`.

Para exibir no navegador, precisamos do component nodejs `http-server`. Que pode ser instalado pela linha de comando com o seguinte comando:

```sh
$ [sudo] npm i -g http-server
```

Após isso, rode um comando para executar no navegador.

```sh
$ npm run start
```

Com isso ele estará disponivel em `localhost:8080`.

## Motivos

Foi feito usando Javascript com AngularJS. Também faz uso do GruntJS para poder concatenar arquivos e minifica-los.

Fiz uso do bower como gerenciador de depedências e foi feito versionamento usando GIT.

Qualquer dúvida em relação aos itens acima pode entrar em contato :)