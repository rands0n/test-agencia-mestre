module.exports = function (grunt) {

    grunt.initConfig({

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: [
                    'bower_components/angular/angular.js',
                    'bower_components/angular-animate/angular-animate.js',
                    'bower_components/angular-cookies/angular-cookies.js',
                    'bower_components/angular-resource/angular-resource.js',
                    'bower_components/angular-route/angular-route.js',
                    'bower_components/angular-sanitize/angular-sanitize.js',
                    'bower_components/angular-touch/angular-touch.js',

                    'app/js/app.js',

                    'app/js/controller/HomeController.js'
                ],
                dest: 'app/dist/app.js'
            }
        },

        watch: {
            js: {
                files: ['app/js/**/*.js', 'Gruntfile.js'],
                tasks: ['concat'],
                options: {
                    spawn: false,
                }
            }
        }

    });

    grunt.registerTask('default', ['concat', 'watch']);

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt);
}