(function(angular) {
    'use strict';

    angular
        .module('app', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch'
        ])
        .config(function ($routeProvider) {
            $routeProvider
              .when('/', {
                templateUrl: 'templates/home.html',
                controller: 'HomeController',
                controllerAs: 'home'
              });
        });
})(window.angular);