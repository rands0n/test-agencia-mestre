(function(angular) {
    'use strict';

    function HomeController ($scope, $http) {

        function _init() {
            getRandomNumbers();
            getResultOfLotoMania();
        }

        function getRandomNumbers() {
            var result = [];

            for(var i = 0; i < 50; i++) {
                result[i] = Math.floor(Math.random() * 100);
            }

            var arr = result.filter(function(e, i) {
                return result.indexOf(e) == i;
            });

            $scope.randomNumbers = arr.sort(function (a, b) {
                return (a - b);
            });
        }

        function getResultOfLotoMania() {
            $http({
                url: 'http://confiraloterias.com.br/api/json/?loteria=lotomania&token=r5NOEOPwsf2fiIt',
                method: 'GET'
            })
            .then(function (req) {
                $scope.resultOfLotoMania = req.data;
            });
        }

        _init();
    }

    angular
        .module('app')
        .controller('HomeController', HomeController);
})(window.angular);